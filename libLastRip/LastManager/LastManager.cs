﻿// LibLastRip - A Last.FM ripping library for TheLastRipper
// Copyright (C) 2007  Jop... (Jonas F. Jensen).
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Collections;

namespace LibLastRip
{
	
	///<summary>
	///
	///</summary>
	public partial class LastManager
	{
		protected System.String UserID;
		protected System.String _Password;
		protected System.String StreamURL;
		protected System.String SessionID;
		protected System.String BaseURL;
		protected System.String BasePath;
		protected System.Boolean Subscripter;
		protected System.String _MusicPath;
		protected System.String _QuarantinePath;
		protected System.String _ExcludeFile;
		public System.String Comment = "Recorded from %s";
		protected System.Boolean _ExcludeNewMusic;
		protected System.Boolean _ExcludeExistingMusic;
		protected System.Boolean _HealthEnabled;
		protected System.String _HealthValue;
		protected const System.String PathSeparator = "/";
		protected const System.Int32 ProtocolBufferSize = 4096;
		protected ConnectionStatus Status = ConnectionStatus.Created;
		
		protected static ArrayList invalidPathChars = getInvalidPathChars();
		protected static ArrayList invalidFilenameChars = getInvalidFilenameChars();
		
		///<summary>
		///Initializes an arraylist which contains the system invalid path chars and additional invalid chars for path creation
		///</summary>
		public static ArrayList getInvalidPathChars() {
			ArrayList chars = new ArrayList();
			chars.AddRange(System.IO.Path.GetInvalidPathChars());
			
			// Path separation chars - should not occur in our album path
			chars.Add('\\');
			chars.Add('/');
			
			// Special characters which are not in system list but are also invalid
			chars.Add(':');
			chars.Add('*');
			chars.Add('?');
			chars.Add('"');
			chars.Add('<');
			chars.Add('>');
			chars.Add('|');
			
			return chars;
		}
		
		public static String replaceUtf8Chars(String value) {
			if (!String.IsNullOrEmpty(value)) {
				// beware: this are utf-8 characters, so this file is now stored to utf8
				return value.
					// Replace('ï', 'i'). // works in windows, so do not replace at the moment
					Replace('ř', 'r'). // r with "dach"
					Replace('š', 's'); // s with "dach"
				
			}
			return value;
		}
		
		///<summary>
		///Initializes an arraylist which contains the system invalid filename chars and additional invalid chars for file creation
		///</summary>
		public static ArrayList getInvalidFilenameChars() {
			ArrayList chars = new ArrayList();
			chars.AddRange(System.IO.Path.GetInvalidFileNameChars());

			// Path separation chars - should not occur in our filename
			chars.Add('\\');
			chars.Add('/');

			// Special characters which are not in system list but are also invalid
			chars.Add(':');
			chars.Add('*');
			chars.Add('?');
			chars.Add('"');
			chars.Add('<');
			chars.Add('>');
			chars.Add('|');
			return chars;
		}
		
		///<summary>
		///Initializes an instance of LastManager, and initiates handshake
		///</summary>
		public LastManager(System.String UserID, System.String Password, System.String MusicPath)
		{
			this.MusicPath = MusicPath;
			
			this.Handshake(UserID, Password);
		}
		
		///<summary>
		///Initializes an instance of LastManager
		///</summary>
		public LastManager(System.String MusicPath)
		{
			this.MusicPath = MusicPath;
		}

		///<summary>
		///Occurs when a handshake request has returned
		///</summary>
		/// <remarks>This event may be called on a seperate thread, make sure to invoke any Windows.Forms or GTK# controls modified in EventHandlers</remarks>
		public event System.EventHandler HandshakeReturn;
		
		public void Handshake()
		{
			if(this.UserID == null || this._Password == null)
			{
				throw new System.Exception("UserName and password needed.");
			}
			
			HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("http://ws.audioscrobbler.com/radio/handshake.php?version=" + "1.1.1" + "&platform=" + "linux" + "&username=" + this.UserID + "&passwordmd5=" + this.Password + "&debug=" + "0" + "&partner=");
			Request.BeginGetResponse(new System.AsyncCallback(this.OnHandshakeReturn), Request);
		}
		
		protected void OnHandshakeReturn(System.IAsyncResult Ar)
		{
			System.Boolean Result = false;
			try {
				//Get Response
				HttpWebRequest Request = (HttpWebRequest)Ar.AsyncState;
				HttpWebResponse Response = (HttpWebResponse)Request.EndGetResponse(Ar);
				
				//Get stream and create StreamReader
				Stream Stream = Response.GetResponseStream();
				StreamReader StreamReader = new StreamReader(Stream, Encoding.UTF8);
				
				//Read data sync, since stream.beginRead is worth the trouble when the connections have been established
				System.String Data = StreamReader.ReadToEnd();
				//Closeing everything related to a connection, and releasing system resources
				StreamReader.Close();
				Stream.Close();
				Response.Close();
				
				Result = this.ParseHandshake(Data);
			} catch (Exception) {
				handleError(true, new ErrorEventArgs("No connection to server."));
			}
			
			if(Result)
			{
				this.Status = ConnectionStatus.Connected;
			}else{
				this.Status = ConnectionStatus.Created;
			}
			
			if(this.HandshakeReturn != null)
				this.HandshakeReturn(this, new HandshakeEventArgs(Result));
		}
		
		///<summary>
		///Gives the Last.FM server a handshake
		///</summary>
		public void Handshake(System.String UserID, System.String Password)
		{
			this.UserID = UserID;
			this.Password = Password;
			this.Handshake();
		}
		
		///<summary>
		///Parses the reponse data from a handshake
		///</summary>
		protected System.Boolean ParseHandshake(System.String Data)
		{
			/* This method is a rewrite from Last-Exit */
			System.String []Lines = Data.Split(new System.Char[] {'\n'});
			System.Boolean Result = false;
			
			foreach(System.String Line in Lines)
			{
				System.String []Opts = Line.Split (new System.Char[] {'='},2);
				if (Opts.Length > 1) {
					switch(Opts[0].ToLower())
					{
						case "session":
							if(Opts[1].ToLower() == "failed")
							{
								Result = false;
							}else{
								Result = true;
								this.SessionID = Opts[1];
							}
							break;
						case "stream_url":
							this.StreamURL = Opts[1];
							break;
						case "subscriber":
							if(Opts[1] == "1")
							{
								this.Subscripter = true;
							}else{
								this.Subscripter = false;
							}
							break;
						case "framehack":
							//Don't know what this is for
							break;
						case "base_url":
							this.BaseURL = Opts[1];
							break;
						case "base_path":
							this.BasePath = Opts[1];
							break;
						default:
							writeLogLine("LastManager.ParseHandshake() Unknown key: " + Opts[0] + " Value: " + Opts[1]);
							break;
					}
				}
			}
			return Result;
		}
		
		///<summary>
		///Gets an md5 hash of the password, or sets the password from hash.
		///</summary>
		public System.String Password
		{
			set
			{
				this._Password = value;
			}
			get
			{
				return this._Password;
			}
		}
		
		public static System.String CalculateHash(System.String Pass)
		{
			/*
				Inspired by Last-Exit-4 another GNU GPL licensed Last.FM client
			 */
			MD5 Hasher = MD5.Create ();
			byte[] Hash = Hasher.ComputeHash (Encoding.Default.GetBytes (Pass));
			StringBuilder StrHash = new StringBuilder ();
			
			for (int i = 0; i < Hash.Length; ++i) {
				StrHash.Append (Hash[i].ToString ("x2"));
			}
			
			return StrHash.ToString ();
		}
		
		///<summary>
		///Gets or set the UserName
		///</summary>
		public System.String UserName
		{
			get
			{
				return this.UserID;
			}
			set
			{
				this.UserID = value;
			}
		}
		
		///<summary>
		///Gets or set the MusicPath
		///</summary>
		public System.String MusicPath
		{
			get
			{
				return this._MusicPath;
			}
			set
			{
				this._MusicPath = value;
			}
		}
		
		///<summary>
		///Gets or set the QuarantinePath
		///</summary>
		public System.String QuarantinePath
		{
			get
			{
				return this._QuarantinePath;
			}
			set
			{
				this._QuarantinePath = value;
			}
		}

		///<summary>
		///Gets or set the ExcludeFile
		///</summary>
		public System.String ExcludeFile
		{
			get
			{
				return this._ExcludeFile;
			}
			set
			{
				this._ExcludeFile = value;
			}
		}

		///<summary>
		///Gets or set the ExcludeNewMusic
		///</summary>
		public System.Boolean ExcludeNewMusic
		{
			get
			{
				return this._ExcludeNewMusic;
			}
			set
			{
				this._ExcludeNewMusic = value;
			}
		}

		///<summary>
		///Gets or set the ExcludeExistingMusic
		///</summary>
		public System.Boolean ExcludeExistingMusic
		{
			get
			{
				return this._ExcludeExistingMusic;
			}
			set
			{
				this._ExcludeExistingMusic = value;
			}
		}
		
		///<summary>
		///Gets or set the HealthEnabled
		///</summary>
		public System.Boolean HealthEnabled
		{
			get
			{
				return this._HealthEnabled;
			}
			set
			{
				this._HealthEnabled = value;
			}
		}

		///<summary>
		///Gets or set the HealthValue
		///</summary>
		public System.String HealthValue
		{
			get
			{
				return this._HealthValue;
			}
			set
			{
				this._HealthValue = value;
			}
		}
		
		///<summary>
		///Gets current connection status
		///</summary>
		public ConnectionStatus ConnectionStatus
		{
			get
			{
				return this.Status;
			}
		}
		
		///<summary>
		///Gets the URL for the Last.FM server
		///</summary>
		protected System.String ServiceURL
		{
			get
			{
				return "http://" + this.BaseURL + this.BasePath + "/";
			}
		}

		///<summary>
		///Remove invalid PathChars from a directory name
		///</summary>
		///<param name="PathName">Directory name from which invalid chars should be removed</param>
		internal static System.String RemoveInvalidPathChars(System.String PathName)
		{
			//can't have null (this should have been prevented in the header for MetaInfo)
			if(PathName == null || PathName == "")
				throw new System.Exception("A directory must have a name, it can't be null");
			
			return LastManager.RemoveChars(PathName, invalidPathChars);
		}
		
		///<summary>
		///Remove invalid filename chars from a filename.
		///</summary>
		///<param name="FileName">FileName from which invalid chars should be removed</param>
		internal static System.String RemoveInvalidFileNameChars(System.String FileName)
		{
			//can't have null
			if(FileName == null || FileName == "")
				throw new System.Exception("A file must have a name, it can't be null");
			
			return LastManager.RemoveChars(FileName, invalidFilenameChars);
		}
		
		///<summary>
		///Replaces chars from an array of invalid chars from an input string with an underline character
		///</summary>
		///<param name="Input">String from which InvalidChars must be removed.</param>
		///<param name="InvalidChars">InvalidChars to be removed from Input string.</param>
		private static System.String RemoveChars(System.String Input, ArrayList InvalidChars)
		{
			System.String Output = "";
			foreach(System.Char TestChar in Input.ToCharArray())
			{
				if (InvalidChars.Contains(TestChar)) {
					Output += '_';
				}
				else {
					Output += TestChar.ToString();
				}
			}
			
			// the utf8-chars must be replaced in any case!
			return replaceUtf8Chars(Output);
		}
	}

	public enum ConnectionStatus
	{
		Created,	//LastManager is created
		Connected,	//LastManager has connection and owns a SessionID
		Recording	//LastManager is connected to stream and is recording
	}
	
	///<summary>
	///EventArgs for a HandshakeReturn event
	///</summary>
	public class HandshakeEventArgs : System.EventArgs
	{
		protected System.Boolean _Success;
		
		internal HandshakeEventArgs(System.Boolean Success)
		{
			this._Success = Success;
		}
		
		///<summary>Boolean indicating if handshake was successfull</summary>
		public System.Boolean Success
		{
			get
			{
				return this._Success;
			}
		}
	}
}
